
[![pipeline status](https://forgemia.inra.fr/sk8/sk8-docs/badges/main/pipeline.svg)](https://forgemia.inra.fr/sk8/sk8-docs/-/commits/main) 

# SK8 - Documentations

## Installer quarto et DiagrammeR
``` r
install.packages(c('quarto', 'DiagrammeR'))
```

## Rendu du site

```r
quarto::quarto_render()
```

ou

``` bash
quarto render
```


Puis ouvrir le ficher **public/index.html**


## Auteurs

SK8 Team

# Les bonnes pratiques de développement Shiny

## Utiliser Rstudio

Pour les débutants, vous pouvez créer une application R/Shiny en
utilisant le template de Rstudio (File -\> New File -\> Shiny Web App).
Vous rentrez le nom de l'application qui sera aussi le nom du dossier
contenant votre application, en local.

::: {.callout-warning appearance="minimal"}
Nous vous conseillons de cocher la case "Multiple File (ui.R/server.R)"
:::

Rstudio crée en local un dossier contenant un template d'application, i.e. 2
fichiers ui.R et server.R.

![création app R/Shiny dans
Rstudio](./img/create_shiny_file.png){width="500px"}

Présentation des fichiers :

-   `ui.R` contient la définition de l'interface graphique de votre
    application
-   `server.R` contient les traitements et la réactivité des éléments de
    votre application

Nous vous conseillons de créer un fichier supplémentaire :

-   `global.R` qui contiendra des définitions globales (par exemple des
    couleurs, de l'encodage) ainsi que les appels (source) de fichiers
    contenant des fonctions R appelées dans la partie server de votre
    application.

## Utilisation des barres de progression

Si certains des calculs faits dans votre application sont longs, il peut
être avantageux d'utiliser une barre de progression afin d'indiquer à
l'utilisateur où en est le calcul et ce qu'il reste à faire.

Un lien vers la documentation:
[https://shiny.rstudio.com/articles/progress.html](https://shiny.rstudio.com/articles/progress.html)

## Ajout d'une page d'accueil

Une page d'accueil au chargement de l'application peut être
intéressante, pour informer les utilisateurs sur l'utilisation de
l'application (par exemple, une explication de l'équation ou de la
méthode utilisée) et donner des détails sur la qualité des données (par
exemple, la date de dernier version des données), ainsi que des messages
de licence.

## Expliciter les messages d'erreurs avec `validate()`

Si une erreur se produit dans l'application, par défaut, Shiny délivre
un message rouge et peu explicatif à l'utilisateur. Ce message est donc
souvent peu utile car il mentionne des choses que le développeur peut
comprendre mais pas l'utilisateur.

Il est recommandé d'utiliser la fonction `validate()` afin d'expliciter
les messages d'erreur que l'utilisateur rencontrera en manipulant
l'application.

Un lien vers la documentation:
[https://shiny.rstudio.com/articles/validation.html](https://shiny.rstudio.com/articles/validation.html)

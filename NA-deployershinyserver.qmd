# Installer Shiny Server


<https://www.digitalocean.com/community/tutorials/how-to-configure-ssh-key-based-authentication-on-a-linux-server-fr>

<https://posit.co/download/shiny-server/>

Install the `shiny` R package

``` bash
sudo apt install r-base
```

``` bash
sudo su - \ -c "R -e \"install.packages('shiny', repos='https://cran.rstudio.com/')\""
```

``` bash
wget https://download3.rstudio.org/ubuntu-18.04/x86_64/shiny-server-1.5.20.1002-amd64.deb
sudo apt install ./shiny-server-1.5.20.1002-amd64.deb
rm shiny-server-1.5.20.1002-amd64.deb
```

``` bash
sudo nano /etc/systemd/system/shiny-server.service
```

```
[Unit]
Description=ShinyServer
[Service]
Type=simple
User=john
Group=john
ExecStart=/bin/bash -c 'exec /opt/shiny-server/bin/shiny-server --pidfile=/home/john/shiny/shiny-server.pid >>
/home/john/shiny/logs/shiny-server.log 2>&1'
KillMode=process
ExecReload=/usr/bin/env kill -HUP $MAINPID
ExecStopPost=/usr/bin/env sleep 5
Restart=on-failure
RestartSec=1
Environment="LANG=C.UTF-8"
Environment="SHINY_LOG_LEVEL=TRACE"
StartLimitInterval=45
StartLimitBurst=3
[Install]
WantedBy=multi-user.target
```

``` bash
sudo nano /etc/logrotate.d/shiny-server
```

```
/home/john/shiny/logs/shiny-server.log {
       rotate 12
       copytruncate
       compress
       missingok
       size 1M
}
```

